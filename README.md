# Fidelia

## Introduction
Python package for simulating electrical loads and for computation and plot of the complex power of the phases of those loads under normal or under fraudulent operations.

For further information, you're invited to read:

- [Characterization of False Positive Occurrences of Undesired Reverse Active Power Flow in Electrical Power Consumer Units](https://ieeexplore.ieee.org/document/8627247)
- [(In Portuguese) Fluxo Reverso de Potência Ativa em Medidores de Energia Elétrica: Uma Contribuição para a Compreensão das Falsas Ocorrências de Irregularidades em Unidades Consumidoras](https://gitlab.com/daniel_pompermayer/tcc-potencia-negativa)

## Built with
 - Emacs
 - Python
 - Numpy
 - Matplotlib

## Development Status
Fidelia is fully running for Delta-connected loads and Wye-connected loads with neutral grounding. Wye-connected loads with isolated neutral seem to present some weird behavior.

## Needed Improvements
This code has been built as a proof of concept. It may need some standardization or optimization. You may feel free to contribute.

## Getting Started
In order to run Fidelia, all you need to do is download its source files, import it under some python script and call its functions passing the needed arguments.

You may also run teste.py directly from terminal, which will run a delta-connected load simulation and a wye-connected with neutral grounding load simulation, sweeping loads with 11 modulus values and 15 argument values of admittance. It will also simulate some irregular behaviors such as inversion of Current Transformer conductors or Sequence Inversion.

## Authors
 - [Daniel Campos Pompermayer](mailto:daniel.pompermayer@ufes.br)
 - [Márcio Almeida Có](mailto:marcio@ifes.edu.br)
 - [Clainer Bravin Donadel](mailto:cdonadel@ifes.edu.br)

## License
Fidelia
Copyright (C) 2019  Instituto Federal do Espírito Santo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
