from fidelia.standards import three_phase_voltages as voltages
from fidelia import load as lo
from fidelia import fraud as fr
from fidelia import plotter as plt
import numpy as np

# Path to save images in.
save_in = ("./Simulations/")

# Delta loads.
delta = lo.LinSet(11, 15, topology="delta", voltages=voltages)
qd = plt.Quadrants(delta.powers, save_in + "delta_quadrants")
qd.save()
td = plt.Total(delta.powers, save_in + "delta_total")
td.save()

# Delta current inverted loads.
ci_delta = fr.cheat(delta, "current_inversion", inversions=[-1, 1, 1, 1])
ci_qd = plt.Quadrants(ci_delta.powers, save_in + "ci_delta_quadrants")
ci_qd.save()
ci_td = plt.Total(ci_delta.powers, save_in + "ci_delta_total")
ci_td.save()

# Delta sequence inverted loads (Phases A and B).
si_delta = fr.cheat(delta, "sequence_inversion", phase_1=0, phase_2=1)
si_qd = plt.Quadrants(si_delta.powers, save_in + "si_delta_quadrants")
si_qd.save()
si_td = plt.Total(si_delta.powers, save_in + "si_delta_total")
si_td.save()

# Delta sequence inverted loads (Phases A and C).
si_ac_delta = fr.cheat(delta, "sequence_inversion", phase_1=0, phase_2=2)
si_ac_qd = plt.Quadrants(si_ac_delta.powers, save_in + "si_ac_delta_quadrants")
si_ac_qd.save()

# Grounding Wye Loads.
grounding_wye = lo.LinSet(11, 15, topology="wye", voltages=voltages)
qgw = plt.Quadrants(grounding_wye.powers, save_in + "grounding_wye_quadrants",
                    axis=[-2, 2, -2, 2])
qgw.save()
tgw = plt.Total(grounding_wye.powers, save_in + "grounding_wye_total",
                axis=[-4, 4, -4, 4])
tgw.save()

# Grounding Wye current inverted loads.
ci_grounding_wye = fr.cheat(grounding_wye, "current_inversion",
                            inversions=[-1, 1, 1, 1])
ci_qgw = plt.Quadrants(ci_grounding_wye.powers, save_in +
                       "ci_grounding_wye_quadrants", axis=[-2, 2, -2, 2])
ci_qgw.save()
ci_tgw = plt.Total(ci_grounding_wye.powers, save_in + "ci_grounding_wye_total",
                   axis=[-4, 4, -4, 4])
ci_tgw.save()

# Grounding Wye sequence inverted loads (phases A and B).
si_grounding_wye = fr.cheat(grounding_wye, "sequence_inversion", phase_1=0,
                            phase_2=1)
si_qgw = plt.Quadrants(si_grounding_wye.powers, save_in +
                       "si_grounding_wye_quadrants", axis=[-2, 2, -2, 2])
si_qgw.save()
si_tgw = plt.Total(si_grounding_wye.powers, save_in + "si_grounding_wye_total",
                   axis=[-4, 4, -4, 4])
si_tgw.save()

# Grounding Wye sequence inverted loads (phases A and C).
si_ac_grounding_wye = fr.cheat(grounding_wye, "sequence_inversion", phase_1=0,
                               phase_2=2)
si_ac_qgw = plt.Quadrants(si_ac_grounding_wye.powers, save_in +
                          "si_ac_grounding_wye_quadrants", axis=[-2, 2, -2, 2])
si_ac_qgw.save()

# Real loads experiment.
powers = np.array([[0, -16.4+61.1*1j, 41.9+47.1*1j],
                   [0, 33.3+87.4*1j, 90.8+23.3*1j],
                   [0, 18.5-31.3*1j, -17.0-31.8*1j],
                   [0, 68.0-5.9*1j, 32.2-59.9*1j],
                   [57.5+75.8*1j, -16.3+61.3*1j, 9.5+128.3*1j],
                   [-16.9-31.2*1j, 18.4-31.1*1j, 1.0-62.0*1j],
                   [-16.9-31.3*1j, -16.2+60.8*1j, 59.6-23.3*1j],
                   [58.2+77.7*1j, 18.6-31.4*1j,	-50.1+63.0*1j],
                   [-35.8+33.7*1j, 24.6+102*1j,	59.4-23.3*1j],
                   [-35.5+34.3*1j, 60.2-24.8*1j, 1-62.3*1j],
                   [40+130*1j, 24.5+104*1j, 9+129*1j],
                   [0.46-58.9*1j, 3.1-59*1j, 1.06-62.4*1j],
                   [98.4+1.3*1j, 99.1+1.3*1j, 98.1+1.3*1j],
                   [108.3+106.2*1j, 68.4-60*1j, -49.7+63*1j]])
