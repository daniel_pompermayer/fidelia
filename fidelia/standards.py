import numpy as np

# Rotational Phasor
alfa = -0.5 + 1j * np.sqrt(3)/2
three_phase_voltages = [1, alfa**2, alfa]
