#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Provide the functions for simulating frauded electrical loads."""
__author__ = "Daniel Campos Pompermayer [gitlab.com/daniel_pompermayer]"


import numpy as np


def current_inversion(matrix, invertions):
    """current_inversion simulates a fraud of invertion of the current
    conductors in an electrical energy meter terminals.

    Inputs are a load's matrix of addmitances and a list with the frauded
    phases. This list must be sized as the number of rows in the matrix. You
    must place a -1 value where you want to invert and a 1 where you want it
    to be the same. Different values will affect the load value.
    """

    return np.multiply(matrix, np.atleast_2d(invertions).T)


def sequence_inversion(matrix, phase_1, phase_2):
    """sequence_inversion simulates a fraud of invertion of the current phase
    sequence par rapport to the voltage phase sequence in an electrical energy
    meter terminals.

    Inputs are a load's matrix of addmitances and the two changed phases. Those
    phases are actualy the index of each phase in the matrix.
    """

    # Make a copy to avoid changing user's matrix.
    my_matrix = matrix.copy()

    # Swap rows.
    temp = my_matrix[..., phase_1, :].copy()
    my_matrix[..., phase_1, :] = my_matrix[..., phase_2, :].copy()
    my_matrix[..., phase_2, :] = temp

    return my_matrix


def cheat(load, kind="current_inversion", **kwargs):
    """Apply a fraud to a load returning a new frauded one. Supported frauds
    are: current_inversion and sequence_inversion.

    Inputs are the load you wanna cheat, the kind of fraud and the arguments
    required for each kind of fraud.
    """

    # Make a copy of my original load since its outer reference won't change
    # inside the function context.
    frauded_load = load.copy()

    # Apply your fraud, you cheater!!
    if kind == "current_inversion":
        frauded_load.matrix = current_inversion(load.matrix,
                                                kwargs["inversions"])

    elif kind == "sequence_inversion":
        frauded_load.matrix = sequence_inversion(load.matrix,
                                                 kwargs["phase_1"],
                                                 kwargs["phase_2"])

    else:
        raise AttributeError("Sorry. Your way of cheating is beyond my ethical"
                             + " limits. Ok, ok, it's just an - yet -"
                             + " unsupported kind of fraud.")

    return frauded_load
