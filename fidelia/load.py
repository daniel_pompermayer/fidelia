#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Provide the classes and methods for building electrical loads."""
__author__ = "Daniel Campos Pompermayer [gitlab.com/daniel_pompermayer]"

import copy
import numpy as np
import itertools as it


class Load(object):
    """The Load class builds an electrical load from its matrix of admittances.
    Such a load must be at least a three-phase load composed by its feeding
    buses there included the neutral one.
    
    A matrix of admittances is a matrix composed by the admittances of a load
    disposed according to the following rule:
    
    1. Imagine a matrix whose columns and indexes are the load's feeding buses.

    2. Excluded the diagonal, place in the intersection of two buses the
    negative of the admittance which link those buses.

    3. When no admittance links two buses, a zero must be placed.

    4. The place on a diagonal must be filled by the admittance between this
    bus and the ground subtracted by the sum of all the other items in the row.

    Example: Let's say we have this beautiful three-phase wye load:
                           ______
                    A|----|______|----|
                             YA       |
                           ______     |N    ______
                    B|----|______|----*----|______|---- GND
                             YB       |      YGND
                           ______     |
                    C|----|______|----|
                             YC

    Its matrix of admittances should be:

                            A    B    C        N
                         -------------------------------
                        A|  YA   0    0       -YA
                        B|  0    YB   0       -YB
                        C|  0    0    YC      -YC
                        N| -YA  -YB  -YC  YGND+YA+YB+YC
"""

    def __init__(self, matrix, voltages=[0, 0, 0], neutral_voltages=None):
        self._lock_neutral_voltages = True
        self.matrix = matrix
        self.voltages = voltages
        self.neutral_voltages = neutral_voltages

    @property
    def currents(self):
        return self._currents

    @property
    def matrix(self):
        return self._matrix

    @matrix.setter
    def matrix(self, value):

        # We can only accept numpy arrays as matrices.
        if not isinstance(value, np.ndarray):
            raise ValueError("Matrix should be a squared numpy 2darray of at"
                             + "least order four. Please check the Load class"
                             + " help.")

        # And loads have squared matrices.
        if value.shape[-2] != value.shape[-1]:
            raise ValueError("Matrix should be a squared numpy 2darray of at"
                             + "least order four. Please check the Load class"
                             + " help.")

        # We're creating at least three-phase loads with a neutral bus.
        if value.shape[-1] < 4:
            raise ValueError("Matrix should be a squared numpy 2darray of at"
                             + " least order four. Please check the Load class"
                             + " help.")
        self._matrix = value
        self.compute_neutral_voltages()
        self.compute_currents()
        self.compute_powers()

    @property
    def neutral_voltages(self):
        return self._neutral_voltages

    @neutral_voltages.setter
    def neutral_voltages(self, value):
        good_types = (int, float, np.float, np.float64, complex)
        try:
            if not all(isinstance(v, good_types) for v in value):
                raise ValueError("The neutral voltage must be either complex,"
                                 + " float or a list of those. It can also be"
                                 + " None.")
        except TypeError:
            if not isinstance(value, good_types + (type(None),)):
                raise ValueError("The neutral voltage must be either complex,"
                                 + " float or a list of those. It can also be"
                                 + " None.")
        if value is not None:
            self._neutral_voltages = value
        else:
            self._lock_neutral_voltages = False
            self.compute_neutral_voltages()
        self.compute_currents()
        self.compute_powers()

    @property
    def num_of_buses(self):
        return self.matrix.shape[-1]-1

    @property
    def powers(self):
        return self._powers

    @property
    def voltages(self):
        """Voltage value of each bus of the load. The neutral bus must not be
        included."""
        return self._voltages

    @voltages.setter
    def voltages(self, value):
        try:
            good_types = (int, float, np.float, np.float64, complex)
            if all(isinstance(v, good_types) for v in value):
                if len(value) != self.num_of_buses:
                    raise ValueError("Ops. You seem to have a "
                                     + str(self.num_of_buses) + " buses load,"
                                     " but have parsed " + str(len(value))
                                     + " voltage values.")
                self._voltages = value
                self.compute_neutral_voltages()
                self.compute_currents()
                self.compute_powers()
            else:
                raise ValueError("The voltage of all the buses must be"
                                 + " either complex or float.")
        except TypeError:
            raise TypeError("You must parse a list with the buses voltage")

    def _compute_neutral_voltage(self, matrix):
        if matrix[-1, -1] == 0:
            return 0
        else:
            return (-1 * (matrix[-1, 0:-1]).dot(self.voltages)/matrix[-1, -1])

    def copy(self):
        return Load(self.matrix, self.voltages, self.neutral_voltages)

    def deepcopy(self, memo):
        return Load(copy.deepcopy(self.matrix, self.voltages,
                                  self.neutral_voltages, memo))

    def compute_currents(self):
        try:

            # Check whether we have a single matrix.
            if len(np.shape(self._matrix)) < 3:
                voltages = np.append(self.voltages, self.neutral_voltages)
                self._currents = self.matrix.dot(voltages)[:-1]

            # If we have multiple matrices, we need to compute one by one.
            else:
            
                # Broadcast the voltages in such a way we can stack the neutral
                # voltages.
                voltages = np.broadcast_to(self.voltages, (
                    np.size(self.neutral_voltages), np.size(self.voltages))).T

                # Stack the neutral voltage of each load.
                voltages = np.vstack([voltages, self.neutral_voltages])

                # Compute each current.
                self._currents = np.array(
                    [self.matrix[n, :, :].dot(voltages[:, n])
                     for n in range(np.size(self.neutral_voltages))])[:, :-1]

                # Compute the ground current
                ground_current = np.sum(self._currents[:-1, :], axis=0)
                self._currents[-1, :] = ground_current
        except AttributeError:
            pass

    def compute_neutral_voltages(self):

        # Check whether we can change the current neutral voltage.
        if self._lock_neutral_voltages:
            return

        try:
            # Check whether we have a single matrix.
            if len(np.shape(self._matrix)) < 3:
                self._neutral_voltages = (
                    self._compute_neutral_voltage(self._matrix))

            # If we have multiple matrices, we need to compute one by one.
            else:

                # Empty list for storing the neutral voltages.
                neutral_voltages = []

                # For each matrix, compute the neutral voltage.
                for n in range(np.shape(self.matrix)[-3]):
                    neutral_voltages.append(
                        self._compute_neutral_voltage(self.matrix[n, :, :]))

                self._neutral_voltages = neutral_voltages
        except AttributeError:
            pass

    def compute_powers(self):
        try:
            self._powers = self.voltages * np.conjugate(self.currents)
        except AttributeError:
            pass


class LinSet(Load):
    """A set of linear distributed three-phase loads. Supports only delta
    and wye loads."""

    def __init__(self, num_of_moduli, num_of_arguments, topology="delta",
                 grounding=np.inf, voltages=[0, 0, 0], neutral_voltages=None,
                 limits=[0, 1, -np.pi/2, np.pi/2], matrix=np.zeros((4, 4))):
        Load.__init__(self, matrix, voltages, neutral_voltages)
        self._limits = limits
        self.grounding = grounding
        self.num_of_arguments = num_of_arguments
        self.num_of_moduli = num_of_moduli
        self.topology = topology

    @property
    def grounding(self):
        return self._grounding

    @grounding.setter
    def grounding(self, value):
        good_types = (int, float, np.float, np.float64, complex)
        if not isinstance(value, good_types):
            raise ValueError("The grouding admittance must be either complex"
                             + " or float.")
        self._grounding = value
        self.gen_matrices()

    @property
    def num_of_arguments(self):
        return self._num_of_arguments

    @num_of_arguments.setter
    def num_of_arguments(self, value):
        if not isinstance(value, int):
            raise ValueError("Only integers are accepted as the number"
                             + "of arguments")
        self._num_of_arguments = value
        self.gen_admittances()
        self.gen_matrices()

    @property
    def num_of_moduli(self):
        return self._num_of_moduli

    @num_of_moduli.setter
    def num_of_moduli(self, value):
        if not isinstance(value, int):
            raise ValueError("Only integers are accepted as the number"
                             + "of moduli")
        self._num_of_moduli = value
        self.gen_admittances()
        self.gen_matrices()

    @property
    def topology(self):
        return self._topology

    @topology.setter
    def topology(self, value):
        if value not in ["delta", "wye"]:
            raise ValueError("The new function supports only delta and wye"
                             + " three-phase loads")
        self._topology = value
        self.gen_matrices()

    def copy(self):
        return LinSet(self.num_of_moduli, self.num_of_arguments, self.topology,
                      self.grounding, self.voltages, self.neutral_voltages,
                      self._limits, self.matrix)

    def deepcopy(self, memo):
        return LinSet(copy.deepcopy(self.num_of_moduli, self.num_of_arguments,
                                    self.topology, self.grounding,
                                    self.voltages, self.neutral_voltages,
                                    self._limits, self.matrix, memo))

    def gen_admittances(self):
        """Build some complex numbers and generate three-phase sets of
        addmitances."""

        try:
            # Create arrays containing the given number of moduli and arguments
            moduli = np.linspace(self._limits[0], self._limits[1],
                                 self.num_of_moduli)
            arguments = np.linspace(self._limits[2], self._limits[3],
                                    self.num_of_arguments)

            # Pair moduli and arguments and generate some comple numbers.
            pairs = it.product(moduli, arguments)
            complex_numbers = [mod * np.exp(1j * arg) for mod, arg in pairs]

            # Combine it exhaustively three by three.
            self.admittances = list(set(it.product(complex_numbers, repeat=3)))
        except AttributeError:
            pass

    def gen_matrices(self):
        """Build some loads from a set of admittances."""
        try:
            self.matrix = np.array([new_matrix(
                a, self.grounding, self.topology) for a in self.admittances])
        except AttributeError:
            pass


def new_matrix(a, grounding=np.inf, topology="delta"):
    """Constructor to load matrices. Supports only delta and wye three-phase
    loads."""
    if topology == "delta":
        return np.array([[a[0]+a[2], -a[0], -a[2], 0],
                         [-a[0], a[0]+a[1], -a[1], 0],
                         [-a[2], -a[1], a[1]+a[2], 0],
                         [0, 0, 0, 0]])
    elif topology == "wye":
        return np.array([[a[0], 0, 0, -a[0]],
                         [0, a[1], 0, -a[1]],
                         [0, 0, a[2], -a[2]],
                         [-a[0], -a[1], -a[2], grounding+a[0]+a[1]+a[2]]])

    else:
        raise ValueError("The new function supports only delta and wye"
                         + " three-phase loads")
    
    
