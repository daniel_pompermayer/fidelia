import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path


class Plotter(object):

    def __init__(self, powers, filename, axis=[-5.5, 5.5, -4.5, 4.5]):
        self.colors = ["blue"]
        self.labels = ["Powers"]
        self.powers = powers
        self.filename = filename
        self.axis = axis

    @property
    def filename(self):
        return self._filename

    @filename.setter
    def filename(self, value):
        # Get a operational system agnostic path.
        self._filename = Path(value)

    @property
    def powers(self):
        return self._powers

    @powers.setter
    def powers(self, value):
        self._powers = value
        self._split()

    def _split(self):
        self.data = [self.powers]

    def save(self, figsize=(7.7, 6.3), mark_size=0.1, arrow_head_width=0.3,
             arrow_head_length=0.3, fc="k", ec="k", legend_markerscale=20):
        """Drawn a scatter plot over a Argand plan to better present
        the spitted power data."""

        # Instance a new figure.
        plt.figure(figsize=figsize)

        # Name labels.
        plt.xlabel("P (p.u.)")
        plt.ylabel("Q (p.u.)")

        # Plot each data.
        for index, datum in enumerate(self.data):
            plt.scatter(np.real(datum), np.imag(datum), s=mark_size,
                        c=self.colors[index], label=self.labels[index])

        # Insert some fake axes.
        plt.arrow(np.dot(self.axis, [0.9, 0.1, 0, 0]), 0,
                  np.dot(self.axis, [-0.8, 0.8, 0, 0]), 0,
                  head_width=arrow_head_width,
                  head_length=arrow_head_length, fc=fc, ec=ec)
        plt.arrow(0, np.dot(self.axis, [0, 0, 0.95, 0.05]),
                  0, np.dot(self.axis, [0, 0, -0.8, 0.9]),
                  head_width=arrow_head_width,
                  head_length=arrow_head_length, fc=fc, ec=ec)
        plt.axis("equal")
        plt.axis(self.axis)

        # # Insert legend on the lower left corner.
        plt.legend(loc="lower left", markerscale=legend_markerscale)

        # Save the plot if the user wants to.
        plt.savefig(str(self.filename), bbox_inches="tight")
        

class Quadrants(Plotter):
    """Slice powers into the 4 quadrants of the Argand Plan."""

    def __init__(self, powers, filename, phase="a",
                 axis=[-5.5, 5.5, -4.5, 4.5]):
        Plotter.__init__(self, powers, filename, axis)
        self.colors = ["r", "navy", "tomato", "steelblue"]
        self.labels = ["Negative Active Power\n with Lagging Power Factor",
                       "Positive Active Power\n with Lagging Power Factor",
                       "Negative Active Power\n with Leading Power Factor",
                       "Positive Active Power\n with Leading Power Factor"]
        self.phase = phase

    @property
    def phase(self):
        return self._phase

    @phase.setter
    def phase(self, value):
        self._phase = value
        self._split()

    def _split(self):
        try:
            phase = ("abc").index(self.phase)
            neg_cap = [p for p in self.powers[:, phase] if (
                np.real(p) < 0) and (np.imag(p) <= 0)]
            pos_cap = [p for p in self.powers[:, phase] if (
                np.real(p) >= 0) and (np.imag(p) <= 0)]
            neg_ind = [p for p in self.powers[:, phase] if (
                np.real(p) < 0) and (np.imag(p) > 0)]
            pos_ind = [p for p in self.powers[:, phase] if (
                np.real(p) >= 0) and (np.imag(p) > 0)]
            self.data = [neg_cap, pos_cap, neg_ind, pos_ind]
        except AttributeError:
            pass


class Signal(Plotter):
    """Slice powers into the negatives and positives total active power."""

    def __init__(self, powers, filename, phase="a",
                 axis=[-5.5, 5.5, -4.5, 4.5]):
        Plotter.__init__(self, powers, filename, axis)
        self.colors = ["tomato", "steelblue"]
        self.labels = ["Negative Total Active Power",
                       "Positive Total Active Power"]
        self.phase = phase

    @property
    def phase(self):
        return self._phase

    @phase.setter
    def phase(self, value):
        self._phase = value
        self._split()

    def _split(self):
        try:
            phase = ("abc").index(self.phase)
            neg = [p[phase] for p in self.powers if sum(p) < 0]
            pos = [p[phase] for p in self.powers if sum(p) >= 0]
            self.data = [neg, pos]
        except AttributeError:
            pass


class Total(Plotter):
    """Slice powers into the negatives and positives total active power."""

    def __init__(self, powers, filename, axis=[-12, 12, -12, 12]):
        Plotter.__init__(self, np.sum(powers, axis=1), filename, axis)
        self.colors = ["tomato", "steelblue"]
        self.labels = ["Negative Total Active Power",
                       "Positive Total Active Power"]

    def _split(self):
        try:
            neg = [p for p in self.powers if p < 0]
            pos = [p for p in self.powers if p >= 0]
            self.data = [neg, pos]
        except AttributeError:
            pass
